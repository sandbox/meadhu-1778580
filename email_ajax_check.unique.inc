<?php

/**
 * @file
 * AJAX callbacks for the email_ajax_check module.
 */

/**
 * Main AJAX function: originality check menu callback.
 */
function email_ajax_check_callback() {
  $output = array();

  $mail = check_plain($_GET['emailAddress']);

  $ret = user_validate_mail($mail);
  if ($ret) {
    $output['allowed'] = FALSE;
    $output['msg'] = $ret;
  }
  else {
    $ret = email_ajax_check_email_is_blocked($mail);
    if ($ret) {
      $output['allowed'] = FALSE;
      $output['msg'] = t('The email %email is not allowed.', array('%email' => $mail));
    }
    else {
      $mail = check_plain($mail);
      $ret = _email_ajax_check_is_user_exists($mail);

      if ($ret) {
        $output['allowed'] = FALSE;
        $output['msg'] = t('The e-mail address %email is already taken.', array('%email' => $mail));
      }
      else {
        $output['allowed'] = TRUE;
      }
    }
  }

  drupal_json_output($output);
}

/**
 * Query user table to check if such email address is already exists.
 */
function _email_ajax_check_is_user_exists($mail) {
  return db_query("SELECT COUNT(u.mail) count FROM {users} u WHERE LOWER(u.mail) = LOWER(:mail)", array(':mail' => $mail))->fetchField();
}

/**
 * Checks for email blocked by user administration.
 */
function email_ajax_check_email_is_blocked($mail) {
  return db_select('users')
    ->fields('users', array('mail'))
    ->condition('mail', db_like($mail), 'LIKE')
    ->condition('status', 0)
    ->execute()->fetchObject();
}
