
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

Current Maintainer: Meadhu<deapge@gmail.com>
 Ajax check email address has been used when register new user.


INSTALLATION
------------

1. Download this module and copy to your modules folder(sites/all/modules)

2. Enable the module and configure admin/config/system/email_ajax_check.

3. Refresh your register page.


Keep enable javascript or this module doesn't work normal.
