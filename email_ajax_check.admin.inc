<?php

/**
 * @file
 * Admin page callbacks for the email_ajax_check module.
 */

/**
 * Menu callback; displays the email_ajax_check module settings page.
 */
function email_ajax_check_settings() {
  $form = array();

  $form['email_ajax_check_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Timer threshold'),
    '#description' => t('Threshold in seconds (ex: 0.5, 1).'),
    '#default_value' => variable_get('email_ajax_check_delay', 1),
  );

  return system_settings_form($form);
}
