(function($) {

  Drupal.behaviors.emailCheck = {
    attach : function(context) {
      $('#email-check-informer:not(.email-check-processed)', context).each(function() {
        var input = $(this).parents('.form-item').find('input');

        Drupal.emailCheck.email = '';
        input.keyup(function() {
          if (input.val() != Drupal.emailCheck.email) {
            clearTimeout(Drupal.emailCheck.timer);
            Drupal.emailCheck.timer = setTimeout(function() {
              Drupal.emailCheck.check(input)
            }, parseFloat(Drupal.settings.emailCheck.delay) * 1000);

            if (!$("#email-check-informer").hasClass('email-check-informer-progress')) {
              $("#email-check-informer").removeClass('email-check-informer-accepted').removeClass('email-check-informer-rejected');
            }

            $("#email-check-message").hide();
          }
        }).blur(function() {
          if (input.val() != Drupal.emailCheck.email) {
            Drupal.emailCheck.check(input);
          }
        });
      }).addClass('email-check-processed');
    }
  };

  Drupal.emailCheck = {};
  Drupal.emailCheck.check = function(input) {
    clearTimeout(Drupal.emailCheck.timer);
    Drupal.emailCheck.email = input.val();

    $.ajax({
      url : Drupal.settings.emailCheck.ajaxUrl,
      data : {
        emailAddress : Drupal.emailCheck.email
      },
      dataType : 'json',
      beforeSend : function() {
        $("#email-check-informer").removeClass('email-check-informer-accepted').removeClass('email-check-informer-rejected').addClass('email-check-informer-progress');
      },
      success : function(ret) {
        if (ret['allowed']) {
          $("#email-check-informer").removeClass('email-check-informer-progress').addClass('email-check-informer-accepted');

          input.removeClass('error');
        } else {
          $("#email-check-informer").removeClass('email-check-informer-progress').addClass('email-check-informer-rejected');

          $("#email-check-message").addClass('email-check-message-rejected').html(ret['msg']).show();
        }
      }
    });
  }
})(jQuery);
